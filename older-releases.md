---
layout: page
title: Older releases
permalink: /releases/older-releases/
---

|         | [ns-3.29](/releases/ns-3-29/) | [ns-3.28](/releases/ns-allinone-3.28.1.tar.bz2) |
| [ns-3.27](/releases/ns-allinone-3.27.tar.bz2) | [ns-3.26](/releases/ns-allinone-3.26.tar.bz2) | [ns-3.25](/releases/ns-allinone-3.25.tar.bz2) |
| [ns-3.24](/releases/ns-allinone-3.24.1.tar.bz2) | [ns-3.23](/releases/ns-allinone-3.23.tar.bz2) | [ns-3.22 ](/releases/ns-allinone-3.22.tar.bz2) |
| [ns-3.21](/releases/ns-allinone-3.21.tar.bz2) | [ns-3.20](/releases/ns-allinone-3.20.tar.bz2) | [ns-3.19 ](/releases/ns-allinone-3.19.tar.bz2) |
| [ns-3.18](/releases/ns-allinone-3.18.2.tar.bz2) | [ns-3.17](/releases/ns-allinone-3.17.tar.bz2) | [ns-3.16 ](/releases/ns-allinone-3.16.tar.bz2) |
| [ns-3.15](/releases/ns-allinone-3.15.tar.bz2) | [ns-3.14](/releases/ns-allinone-3.14.1.tar.bz2) | [ns-3.13 ](/releases/ns-allinone-3.13.tar.bz2) |
| [ns-3.12](/releases/ns-allinone-3.12.1.tar.bz2) | [ns-3.11](/releases/ns-allinone-3.11.tar.bz2) | [ns-3.10 ](/releases/ns-allinone-3.10.tar.bz2) |
| [ns-3.9](/releases/ns-allinone-3.9.tar.bz2) | [ns-3.8](/releases/ns-allinone-3.8.tar.bz2) | [ns-3.7 ](/releases/ns-allinone-3.7.1.tar.bz2) |
| [ns-3.6](/releases/ns-allinone-3.6.tar.bz2) | [ns-3.5](/releases/ns-allinone-3.5.1.tar.bz2) | [ns-3.4 ](/releases/ns-allinone-3.4.tar.bz2) |
| [ns-3.3](/releases/ns-3.3.tar.bz2) | [ns-3.2](/releases/ns-3.2.1.tar.bz2) | [ns-3.1 ](/releases/ns-3.1.tar.bz2) |
