---
layout: post
title:  "Google Summer of Code 2018"
date:   2018-02-18 13:55:18 -0700
categories: news
excerpt_separator: <!--more-->
---

ns-3 is excited to have been selected to the 2018 edition of Google Summer of Code.<!--more--> This program is a great opportunity for students to learn a bit about software engineering and open source projects, and for our project community to grow. Interested students are encouraged to interact with the project through the main project mailing list, ns-developers@isi.edu, and to review our wiki. Students will have until March 27 to develop a project proposal and submit it to Google.
