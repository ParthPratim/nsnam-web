---
title: Mercurial
layout: page
permalink: /develop/tools/mercurial/
---
[Mercurial](http://mercurial.selenic.com/) is an open-source source code management tool that is available on a wide variety of platforms (windows, OSX, Linux, etc.).

A lot of documentation about how to use mercurial can be found elsewhere:

  * If you are new to source code management tools, we suggest that you start with the [mercurial book](http://hgbook.red-bean.com/) which is both an extensive introduction to source code management, mercurial concepts, as well as a reference on how to use mercurial efficiently.
  * If you are already familiar with tools such as cvs, svn, or git but not mercurial, keeping around the cheat sheet (such as [this](http://mercurial.selenic.com/wiki/QuickReferenceCardsAndCheatSheets?action=AttachFile&do=view&target=Mercurial-Cheatsheet-Adrian-v1.0.pdf) or [this](http://www.ivy.fr/mercurial/ref/v1.0/)) while reading the following is helpful.
  * If you are knowledgeable about mercurial but have some questions on how to use it, consult its extensive documentation available from the [main website](http://mercurial.selenic.com/) and the [wiki](http://mercurial.selenic.com/wiki)

<div id="toc_container" class="toc_wrap_right no_bullets">
  <ul class="toc_list">
    <li>
      <a href="#The_url_to_ns-3_repositories"><span class="toc_number toc_depth_1">1</span> The url to ns-3 repositories</a>
    </li>
    <li>
      <a href="#Undoing_local_changes_before_commit"><span class="toc_number toc_depth_1">2</span> Undoing local changes before commit</a>
    </li>
    <li>
      <a href="#Undoing_a_commit"><span class="toc_number toc_depth_1">3</span> Undoing a commit</a>
    </li>
    <li>
      <a href="#Renaming_a_file"><span class="toc_number toc_depth_1">4</span> Renaming a file</a>
    </li>
    <li>
      <a href="#Synchronizing_with_REMOTE-URL"><span class="toc_number toc_depth_1">5</span> Synchronizing with REMOTE-URL</a>
    </li>
    <li>
      <a href="#Setting_the_username_in_commit_logs"><span class="toc_number toc_depth_1">6</span> Setting the username in commit logs</a>
    </li>
    <li>
      <a href="#Committing_a_change_as_another_user"><span class="toc_number toc_depth_1">7</span> Committing a change as another user</a>
    </li>
    <li>
      <a href="#Avoid_using_LONG-URL"><span class="toc_number toc_depth_1">8</span> Avoid using LONG-URL</a>
    </li>
    <li>
      <a href="#Sending_a_commit_to_X"><span class="toc_number toc_depth_1">9</span> Sending a commit to X</a>
    </li>
    <li>
      <a href="#Sending_un-commited_changes_to_X"><span class="toc_number toc_depth_1">10</span> Sending un-commited changes to X</a>
    </li>
  </ul>
</div>

# <span id="The_url_to_ns-3_repositories">The url to ns-3 repositories</span>

The ns-3 server which hosts our mercurial repositories allow read-only access for all repositories through http and read-write access through ssh.

The read-only url for all our repositories is either _http://code.nsnam.org/REPO_ or _http://code.nsnam.org/USER/REPO_. All of these repositories can be listed by visiting this [url](http://code.nsnam.org).

The read-write access for all our repositories takes two forms: _ssh://code@code.nsnam.org/REPO_ or _ssh://USER@code.nsnam.org/repositories/USER/REPO_. To obtain read-write access to one of these repositories, one should send an ssh public key on the [ns-developers](/developers/tools/mailing-lists#ns-developers) mailing-list.

# <span id="Undoing_local_changes_before_commit">Undoing local changes before commit</span>

<tt>hg revert [FILE/DIR]</tt>: revert the state of your local checkout to what it was upon your last update.

# <span id="Undoing_a_commit">Undoing a commit</span>

There are two ways to do this:

  * If you have already published your repository and others have cloned it, you should use <tt>hg backout -r REV</tt> to commit a new change in the mercurial history which undos the previous commit.
  * If you don't care about wreaking havoc on your users who cloned your repository or if you have never published it, you could use <tt>hg rollback</tt> to remove the last commit from the mercurial history. BEWARE that this operation loses history and is undoable. Use with caution.

# <span id="Renaming_a_file">Renaming a file</span>

<tt>hg rename old-file-name new-file-name</tt> This is preferable to adding the new file name and removing the old file name, because it preserves revision history. Don't forget to commit once you are done.

# <span id="Synchronizing_with_REMOTE-URL">Synchronizing with REMOTE-URL</span>

If you have created a local clone of an ns-3 repository, if you have worked on it, commited changes to that local repository, and if you want to synchronize with a more recent version of the original ns-3 repository, here are the steps to take:

  * <tt>cd your-clone</tt>
  * <tt>hg pull REMOTE-URL</tt>: download new history from REMOTE-URL.
  * <tt>hg merge</tt>: attempt to reconcile your local changes with remote changes
  * Check that it still works: builds, tests pass, etc.
  * <tt>hg commit -m "merge with REMOTE-URL"</tt>: commit the reconciled version
  * <tt>hg push OTHER-URL</tt>: finally, push your new history in a published repository.

# <span id="Setting_the_username_in_commit_logs">Setting the username in commit logs</span>

Each mercurial checkin is made by a user. By default, mercurial uses <tt>accountname@hostname</tt> as a username for each commit. This leads to commit logs like the following:

<pre>changeset:   7:e53ac3c458e9
 user:        tomh@powerbook.local
</pre>

To avoid this, and have it print something nicer, like

<pre>changeset:   7:e53ac3c458e9
 user:        Tom Henderson &lt;tomh@tomh.org>
&lt;/tomh@tomh.org></pre>

you need to add an .hgrc file to your home directory, such as follows:

<pre>[ui]
 username = Tom Henderson &lt;tomh@tomh.org>
&lt;/tomh@tomh.org></pre>

# <span id="Committing_a_change_as_another_user">Committing a change as another user</span>

If you, as a maintainer, check in a patch authored by someone else, it is good practice to credit them in the commit message using the &#8211;user or -u option to hg; e.g.:

<pre>hg commit -m "...commit message..." -u "A User &lt;a.user@example.com>"
&lt;/a.user@example.com></pre>

Background information on the practice of [recording contributors](http://www.gnu.org/prep/maintain/html_node/Recording-Contributors.html).

# <span id="Avoid_using_LONG-URL">Avoid using LONG-URL</span>

If you are getting tired of repeating the same LONG-URL in clone, pull, or push commands, you can define shortcuts in your <tt>.hgrc</tt> file:

<pre>[paths]
  my-repo = A-LONG-URL
</pre>

And, then, you can do <tt>hg clone my-repo</tt>

# <span id="Sending_a_commit_to_X">Sending a commit to X</span>

If you want to send to someone the content of a single commit, use <tt>hg export -o FILENAME REV </tt> and send the resulting file FILENAME by email.

# <span id="Sending_un-commited_changes_to_X">Sending un-commited changes to X</span>

If you want to send to someone your local uncommited changes for review, use <tt>hg diff > FILENAME</tt> and send the resulting file FILENAME by email.
