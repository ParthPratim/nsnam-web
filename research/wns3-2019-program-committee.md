---
layout: page
title: Program Committee
permalink: /research/wns3/wns3-2019/program-committee/
---

# General Chair

* Tommaso Pecorella, DINFO - UniFi, <tommaso.pecorella@unifi.it>

# Technical Program Co-Chairs

* Matthieu Coudron, IIJ-II, <mattator@gmail.com>
* Damien Saucez, Inria, <damien.saucez@inria.fr>

# Proceedings Chair

* Eric Gamess, Jacksonville State University, <egamess@gmail.com>

# Technical Program Committee

To be listed at a later date.

