---
layout: page
title: Consortium
permalink: /consortium/
---

The NS-3 Consortium is a collection of organizations cooperating to support and develop the ns-3 software. It operates in support of the open source project by providing a point of contact between industrial members and ns-3 developers, by sponsoring events in support of ns-3 such as users' days and workshops, by guaranteeing maintenance support for ns-3's core, and by supporting administrative activities necessary to conduct a large open source project.

Membership to the Consortium is open to those institutions that sign the membership agreement.  The Consortium is overseen by a Steering Committee composed of individuals appointed by Executive Members of the Consortium. The Consortium is governed by an agreement established between the founding members:  [INRIA](http://www.inria.fr/centre/sophia/) and the [University of Washington](https://www.washington.edu/).

The Consortium sponsors the annual Workshop on ns-3, which  we are pleased to announce will be held on June 19-20, 2019, in Florence Italy, hosted by 
[DINFO - UniFi](https://www.dinfo.unifi.it/changelang-eng.html).  More details will be posted soon, including a Call for Papers.
