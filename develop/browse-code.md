---
layout: page
title: Browse code
permalink: /develop/browse-code/
---
The ns-3 project maintains a dedicated server to host various mercurial repositories that contain the ns-3 source code. The main development tree can be easily [browsed online](http://code.nsnam.org/ns-3-dev).

Every repository hosted on our server can also be [seen there](http://code.nsnam.org/).
