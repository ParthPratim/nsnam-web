---
title: Licensing
layout: page
permalink: /develop/contributing-code/licensing/
---
# Licensing

All code submitted must conform to the project licensing framework, which is [GNU GPLv2](http://www.gnu.org/licenses/gpl-2.0.html) compatibility. All new source files must contain a license statement. All modified source files must either conform to the license of the original file or (in certain cases described below) may add a license to the original license.

Note that it is incumbent upon the submitter to make sure that the licensing attribution is correct and that the code is suitable for ns-3 inclusion. _Do not_ include code (even snippets) from sources that have incompatible licenses.

# Copyright and license of new files

If you are writing a new file, include a GPLv2 license with your copyright statement in the header, such as: <tt>Copyright 2009 John Doe</tt>. Do not use the phrase <tt>All rights reserved</tt>, because you are granting certain rights to the user by licensing it to them under GPLv2. If you work for an employer or a university and the work was paid for under a project, you will want to check with your employer as to how to properly identify the copyright holder (which may not be you as an individual). Please contact the maintainers if you have a particular reason to provide your new work with a license other than GPLv2, and the project will take it into consideration.

# Modification of existing files

If you make significant modifications to an existing file in ns-3, you may, at your option, add a copyright statement below the original copyright statement. The definition of significant modifications is open to interpretation.

# Authorship

Optionally, when making a significant modification or when the copyright holder is an institution, you may add an author line to help us track you down in case of problems in the code such as <tt>Author: john_doe@example.edu</tt> or <tt>Modifications made by: john_doe@example.edu</tt>. However, for small modifications, please do not create a large chain of authors by appending to this list in the header; we can use Mercurial change history to log who has changed what file.

# Including unmodified permissive-licensed files

If you wish to include an unmodified file that is permissively licensed in a manner compatible with GPLv2, such as a small utility library, please follow the guidelines posted at the SFLC [(Section 2.1)](http://www.softwarefreedom.org/resources/2007/gpl-non-gpl-collaboration.html). We do not have a current instance of this in the ns-3 codebase.

# Porting of permissively licensed files

If you port a file under a different license that is compatible with GPLv2, please follow the guidelines posted at the SFLC [(Section 2.2)](http://www.softwarefreedom.org/resources/2007/gpl-non-gpl-collaboration.html). In particular, this practice should be followed when porting files from ns-2 that are under the modified BSD license (much of ns-2 is licensed with modified BSD). See the file <tt>src/network/utils/error-model.h</tt> for an example.
