---
title: Release process
layout: page
permalink: /develop/release-process/
---
ns-3 releases are based on date-driven schedules: rather than target a set of features for a specific version number, we aim instead for a release date and ship whatever is ready by that date. If a cool new feature misses that date, it is not a big deal because the next release is never too far away. This interval is typically 3-4 months.

The objective of this deterministic and predictable release rhythm is to allow external contributors to synchronize their own development schedule on the ns-3 release schedule and to be able to plan their contributions accordingly. This relieves the ns-3 maintainers from the burden of having to make strong unpopular decisions about the merging of new features and allows them to focus instead on maintaining the ns-3 models to achieve the highest-quality possible releases.

An ns-3 release manager manages each release. Following the ns-3.X release, a new release manager is selected for the ns-3.X+1 release. The old ns-3.X release manager is typically responsible for any maintenance ns-3.X.Y releases. The Release Manager is allowed to veto and remove any new feature addition if it begins to cause problems and looks like it threatens the stability of the release at any time in the release process.

Each release is roughly structured as follows: large major intrusive changes that are the most destabilizing to the codebase are always merged first early on during the cycle. As we approach the release deadline, the number and the size of the changes should decrease until only high-priority bugs are fixed during the _hard freeze_ period.

[<img alt="The ns-3 release process" class="size-full wp-image-350" height="294" src="http://www.nsnam.org/wp-content/uploads/2011/01/release-process.png" title="The ns-3 release process" width="510" srcset="https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2011/01/release-process.png 510w, https://nsnam-www2.coe-hosted.gatech.edu/wp-content/uploads/2011/01/release-process-300x172.png 300w" sizes="(max-width: 510px) 100vw, 510px" />](http://www.nsnam.org/wp-content/uploads/2011/01/release-process.png) 

During the open phase, people wanting to merge a new feature should contact the Release Manager and arrange to have their features merged into the main development tree. You will be expected to follow the [code submission guidelines](http://www.nsnam.org/developers/contributing-code/). One of the ns-3 maintainers will take a quick look at your proposed addition and determine if a code review is required.

According to the book of instructions a code review requiring positive acknowledgment by maintainers is indicated if:

  * Your proposed feature does not work with all models or on all platforms
  * Your feature changes pre-existing APIs
  * Your feature crosses maintainer boundaries

Just to be safe, we will probably run a feature submission by at least one maintainer according to the general area of applicability of the feature. For example, if you submit an entirely new device driver model, as a courtesy we will run this submission by the maintainers of the current devices. The maintainers won't have any responsibility to positively ack the submission, but we will take some time to allow a reasonable review.

During the feature-freeze (also called maintenance) phase, no new features may be added, but the maintainers may check in fixes to bugs; and people with new features that have been accepted and previously merged may fix bugs in existing features. Please don't try to sneak in more new features or you may have your whole feature set removed at the release manager's discretion. You can ask if you want to add small, self-contained features, but there are no guarantees that we will okay them.

During the _Hard code freeze_, our primary goal is stability: only P1 bugfixes will be allowed to be checked in. Our goal will be to reduce the number of P1 bugs to zero before the release.
