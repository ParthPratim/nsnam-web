---
layout: page
title: WNS3 2018
permalink: /research/wns3/wns3-2018/
---

The **Workshop on ns-3 (WNS3)** was a one and one-half day workshop that was held on June 13-14, 2018, hosted by the [National Institute of Technology Karnataka (NITK) Surathkal](http://www.nitk.ac.in/), Mangalore, India. The objective of the workshop is to gather ns-3 users and developers, together with networking simulation practitioners and users, and developers of other network simulation tools, to discuss the ns-3 simulator and related activities. The Call for Papers was published on a [separate page](/research/wns3/wns3-2018/call-for-papers/), and an [annual meeting flyer](https://www.nsnam.org/workshops/wns3-2018/ns-3-annual-meeting-2018.pdf) was previously posted.

On Monday and Tuesday, ns-3 training was offered; more details are on the [Training page](/research/wns3/wns3-2018/training/). The schedule details for Wednesday through Friday are available on the [Program page](/research/wns3/wns3-2018/program).

Proceedings are published in the [ACM digital library](https://dl.acm.org).
