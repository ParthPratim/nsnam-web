---
title: Writing scripts
layout: page
permalink: /support/faq/writing-scripts/
---
# Starting from an existing script

While experienced users of ns-3 often write new simulation scripts from scratch, most users merely copy/paste an existing script which incorporates the simulation models they need and modify it until it matches what they want. Consult the extensive example scripts that ship with every module.

# Building a new C++ script

Once you have identified which simulation script you would like to start from, building a modified version of this script is a matter of dropping it in the _scratch_ directory and running waf again:

<pre>cp examples/csma/csma-broadcast.cc scratch/csma-modified.cc
./waf
</pre>

Running a new script which was dropped in the scratch directory is similar to [running other examples](/support/faq/running-scripts).

# Building many C++ files

If your simulation script becomes complex and if you split it in more than one C++ source file, you need to create a new sub-directory in the _scratch_ directory:

<pre>mkdir scratch/modified
cp x.cc scratch/modified
cp y.cc scratch/modified
./waf 
</pre>

This will build a new program named after the name of your sub-directory (_modified_ here) and you can run it just like any other example:

<pre>./waf --run modified
</pre>
