---
title: IRC
layout: page
permalink: /develop/tools/irc/
---
The #ns-3 IRC channel is hosted on the [irc.freenode.net](http://freenode.net) network. Feel free to join and chat to discuss ns-3. While any IRC client would do just fine, we often use the [freenode web interface](http://webchat.freenode.net?channels=ns-3).
